// Import Core Module http
const http = require('http');
const fs = require('fs');

fs.readFile('./index.html', function (err, html) {
    if (err) {
        throw err;
    }
    //Method createServer itu bawaan module http
    //Server mengandung 2 parameter: request dan response
    http.createServer(function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(html);
    }).listen(5000);
    //Port yang penting tidak bentrok
})
